package com.cch.platform.core.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.core.bean.CorePrefData;
import com.cch.platform.core.bean.CorePreference;
import com.cch.platform.service.BaseService;

@Service
public class PreferenceService extends BaseService<CorePreference>{

	public String getPreference(String prefCode,BaseUser user){
		CorePreference pref=this.get(prefCode);
		if(pref==null||user==null) return null;
		String type=pref.getPrefType(),owner = "";
		if("global".equals(type)){
			return pref.getDefaultValue();
		}else if("user".equals(type)){
			owner=user.getUserId().toString();
		}else if("role".equals(type)){
			owner=user.getRoleId().toString();
		}
		CorePrefData pd=new CorePrefData(prefCode, owner);
		pd=this.get(CorePrefData.class,pd);
		if(pd!=null) return pd.getPrefValue();
		return "";
	}
	
	public void saveValue(String prefCode,String prefValue,BaseUser user){
		CorePreference pref=this.get(prefCode);
		String type=pref.getPrefType(),owner = "";
		if("global".equals(type)){
			pref.setDefaultValue(prefValue);
			this.saveOrUpdate(pref);
			return;
		}else if("user".equals(type)){
			owner=user.getUserId().toString();
		}else if("role".equals(type)){
			owner=user.getRoleId().toString();
		}
		CorePrefData pd=new CorePrefData(prefCode, owner);
		pd.setPrefValue(prefValue);
		this.saveOrUpdate(pd);
	}
	
}
