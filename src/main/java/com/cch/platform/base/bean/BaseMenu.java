package com.cch.platform.base.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BaseMenu entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "base_menu")
public class BaseMenu implements java.io.Serializable {

	// Fields

	private String code;
	private String pcode;
	private String name;
	private Integer sort;
	private String url;
	private String picture;

	// Constructors

	/** default constructor */
	public BaseMenu() {
	}

	/** minimal constructor */
	public BaseMenu(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/** full constructor */
	public BaseMenu(String code, String pcode, String name, Integer sort,
			String url, String picture) {
		this.code = code;
		this.pcode = pcode;
		this.name = name;
		this.sort = sort;
		this.url = url;
		this.picture = picture;
//		System.getenv()
	}

	// Property accessors
	@Id
	@Column(name = "code", unique = true, nullable = false, length = 32)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "pcode", length = 32)
	public String getPcode() {
		return this.pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	@Column(name = "name", nullable = false, length = 128)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name = "url", length = 500)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "picture", length = 500)
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

}