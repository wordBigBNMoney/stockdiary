package com.cch.stock.set.web;

import java.util.Date;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.util.DateUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.set.bean.SdStock;
import com.cch.stock.set.service.StockPriceService;

@Controller
@RequestMapping(value = "/stock/stockprice")
public class StockPrice {

	@Autowired
	private StockPriceService service;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pagequery.do")
	public ModelAndView  pageQuery(ServletRequest request) {
		Map<String, Object> param=WebUtil.getParam(request);
		return new ModelAndView("jsonView",service.pageQuery(param));
	}

	@RequestMapping(value = "/updateprice.do")
	public ModelAndView  updatePrice(ServletRequest request) throws Exception {
		Map<String, Object> param=WebUtil.getParam(request);
		Date from=DateUtil.parseDate((String) param.get("dateFrom"));
		Date to=DateUtil.parseDate((String) param.get("dateTo"));
		service.updatePrice((String) param.get("stockCode"),from,to);
		return WebUtil.sucesseView("同步成功");
	}
}
