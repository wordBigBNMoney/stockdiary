<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./moneyreport.js"></script>
	<script src="../../core/highcharts/highstock.js"></script>
	
</head>

<body>
	<div  id="summary">
		<form id="ff" method="post" ajax="true">
			<table cellpadding="5">
				<tr>
					<td>从:</td>
					<td><input name='dateFrom' class="easyui-datebox"/></td>
					<td>到:</td>
					<td><input name='dateTo' class="easyui-datebox"/></td>
					<td><a class="easyui-linkbutton" iconCls="icon-search" onclick="getReport()">查询</a></td>
				</tr>
				<tr>
					<td>期初：</td>
					<td><input class="easyui-numberbox" name="beginmny" precision="2"/></td>
					<td>期末：</td>
					<td><input class="easyui-numberbox" name="endmny" precision="2"/></td>
				</tr>
				<tr>
					<td>转入：</td>
					<td><input class="easyui-numberbox" name="inmny" precision="2" /></td>
					<td>转出：</td>
					<td><input class="easyui-numberbox" name="outmny" precision="2"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="container">
		查询中.......
	</div>
</body>
</html>
